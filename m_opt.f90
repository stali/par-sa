module opt

  use mpi

contains

  ! The routine below is a parallel implementation of Alan Miller's serial 
  ! program 'simann.f90' (available online) which was originally written by
  ! Bill Goffe and is based on Corana's algorithm
  !
  ! - The intermediate loop 'ns' has been parallelized using MPI
  !
  ! - To choose appropriate values for annealing parameters see 'simann.f90'
  ! 
  ! tabrezali@gmail.com

  subroutine sa(fcn, n, x, maxm, rt, eps, ns, nt, neps, maxevl, lb, ub, c, t,  &
     vm, xopt, fopt, nacc, nfcnev, nobds, stat)

    implicit none
    external :: fcn
    real(8), intent(in)     :: lb(:), ub(:), c(:), eps, rt
    real(8), intent(in out) :: x(:), t, vm(:)
    real(8), intent(out)    :: xopt(:), fopt
    integer, intent(in)     :: n, neps, maxevl
    integer, intent(in out) :: ns, nt
    integer, intent(out)    :: nacc, nfcnev, nobds, stat
    logical, intent(in)     :: maxm

    ! Internal variables
    real(8) :: randn, f, fp, p, pp, ratio, xp(n), fstar(neps)
    integer :: h, i, j, m, nacp(n), lns, sum_int, sum_int_n(n)
    logical :: quit

    ! MPI stuff
    integer :: ierr, myrank, nprocs
    real(8) :: val_rank(2), max_val_rank(2)

    call MPI_Comm_rank(MPI_COMM_WORLD, myrank, ierr)
    call MPI_Comm_size(MPI_COMM_WORLD, nprocs, ierr)
    stat = -1

    if (nprocs <= ns) then

       nacc = 0
       nobds = 0
       nfcnev = 0
       xopt = x
       nacp = 0
       fstar = 1.0d+20
       call fcn(n, x, f)
       if (.not. maxm) f = -f
       nfcnev = nfcnev + 1
       fopt = f
       fstar(1) = f

       if (nprocs <= ns) lns = int(ceiling(real(ns)/real(nprocs)))

100    do m = 1, nt
          do j = 1, lns
             do h = 1, n

                ! Generate xp, the trial value of x; Note the use of vm to 
                ! choose xp
                do i = 1, n
                   if (i == h) then
                      call random_number(randn)
                      xp(i) = x(i) + (randn*2.0 - 1.0) * vm(i)
                   else
                      xp(i) = x(i)
                   end if
                   ! If xp is out of bounds, select a point within bounds for
                   ! the trial
                   if ((xp(i) < lb(i)) .or. (xp(i) > ub(i))) then
                      xp(i) = lb(i) + (ub(i) - lb(i)) * randn
                      nobds = nobds + 1
                   end if
                end do

                ! Evaluate the function with the trial point xp and return fp
                call fcn(n, xp, fp)
                if (.not. maxm) fp = -fp
                nfcnev = nfcnev + 1

                ! If too many function evaluations occur, terminate
                if (nfcnev >= maxevl) then
                   if (.not. maxm) fopt = -fopt
                   if (myrank == 0) print*, "ERROR: Exceeded 'maxevl' i.e.,",  &
                      maxevl
                   return
                end if

                ! Accept the new point if the function value increases
                if (fp >= f) then
                   x = xp
                   f = fp
                   nacc = nacc + 1
                   nacp(h) = nacp(h) + 1
                   ! If greater than any other point, record as new optimum
                   if (fp > fopt) then
                      xopt = xp
                      fopt = fp
                   end if
                else
                   ! If the point is lower, use the metropolis criteria to
                   ! decide on acceptance or rejection
                   p = exp((fp - f)/t)
                   pp = randn
                   if (pp < p) then
                      x = xp
                      f = fp
                      nacc = nacc + 1
                      nacp(h) = nacp(h) + 1
                   end if
                end if

             end do
          end do

          ! Adjust vm so that approximately half of all evaluations are accepted
          call MPI_Reduce(nacp, sum_int_n, n, MPI_INTEGER, MPI_SUM, 0,         &
             MPI_COMM_WORLD, ierr); nacp = sum_int_n
          if (myrank == 0) then
             do i = 1, n
                ratio = dble(nacp(i))/dble(ns)
                if (ratio > .6) then
                   vm(i) = vm(i)*(1. + c(i)*(ratio - .6)/.4)
                else if (ratio < .4) then
                   vm(i) = vm(i)/(1. + c(i)*(.4 - ratio)/.4)
                end if
                if (vm(i) > (ub(i)-lb(i))) then
                   vm(i) = ub(i) - lb(i)
                end if
             end do
          end if
          call MPI_Bcast(vm, n, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)

          nacp = 0

       end do

       ! Update fopt/xopt
       val_rank(1) = fopt; val_rank(2) = myrank
       call MPI_Allreduce(val_rank, max_val_rank, 1, MPI_2DOUBLE_PRECISION,    &
          MPI_MAXLOC, MPI_COMM_WORLD, ierr)
       fopt = max_val_rank(1)
       call MPI_Bcast(xopt, n, MPI_DOUBLE_PRECISION, int(max_val_rank(2)),     &
          MPI_COMM_WORLD, ierr)

       ! Check termination criteria
       if (myrank == 0) then
          quit = .false.
          fstar(1) = f
          if ((fopt - fstar(1)) <= eps) quit = .true.
          do i = 1, neps
             if (abs(f - fstar(i)) > eps) quit = .false.
          end do
       end if
       call MPI_Bcast(quit, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)

       ! Terminate SA if appropriate
       if (quit) then
          if (.not. maxm) fopt = -fopt
          stat = 0
          call MPI_Reduce(nfcnev, sum_int, 1, MPI_INTEGER, MPI_SUM, 0,         &
             MPI_COMM_WORLD, ierr); nfcnev = sum_int
          call MPI_Reduce(  nacc, sum_int, 1, MPI_INTEGER, MPI_SUM, 0,         &
             MPI_COMM_WORLD, ierr); nacc = sum_int
          call MPI_Reduce( nobds, sum_int, 1, MPI_INTEGER, MPI_SUM, 0,         &
             MPI_COMM_WORLD, ierr); nobds = sum_int
          return
       end if

       ! If termination criteria is not met, prepare for another loop
       t = rt*t
       do i = neps, 2, -1
          fstar(i) = fstar(i-1)
       end do
       f = fopt
       x = xopt
       go to 100

    else
       if (myrank == 0) print*, "ERROR: Scales up to 'Ns' i.e.,", ns, "procs"
    end if

  end subroutine sa

end module opt
