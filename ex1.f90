program sa_demo

  use opt

  implicit none

  integer, parameter :: n = 2, neps = 4
  real(8)  :: lb(n), ub(n), x(n), xopt(n), c(n), vm(n), t, eps, rt, fopt
  integer  :: ns, nt, nfcnev, maxevl, nacc, nobds, stat, ierr, rank, nprocs,   &
              seed(64)
  logical  :: maxm
  external :: fcn

  call MPI_Init(ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nprocs, ierr)

  seed = rank
  call random_seed(put=seed)

  ! Set input parameters.
  maxm = .true.
  t = 20.0
  rt = 0.5
  vm = 1.0
  eps = 1.0d-3
  nt = 10
  ns = 20
  maxevl = 100000
  c = 2.0

  x(1) = 0.0; x(2) = 3.0 ! Start at local optima
  lb(1) = -1.5; ub(1) = 2.0
  lb(2) = -0.5; ub(2) = 3.0


  call sa(fcn, n, x, maxm, rt, eps, ns, nt, neps, maxevl, lb, ub, c, t, vm,    &
     xopt, fopt, nacc, nfcnev, nobds, stat)

  if (stat == 0 .and. rank == 0) then
     write(*,1000) fopt, nfcnev, nacc, nobds, nfcnev/nprocs, t
1000 format(' Optimal function value: ',g26.7                                  &
          /,' Number of function evaluations:     ',i10,                       &
          /,' Number of accepted evaluations:     ',i10,                       &
          /,' Number of out of bound evaluations: ',i10,                       &
          /,' Evaluations per CPU ~               ',i10,                       &
          /,' Final temperature         ',       g20.10); print*, ""
     print*, "Global Xopt is at:", real(xopt)
  end if

  call MPI_Finalize(ierr)

end program sa_demo


subroutine fcn(n, theta, h)
  ! 2D Rosenbrock function
  implicit none
  integer, intent(in)  :: n
  real(8), intent(in)  :: theta(n)
  real(8), intent(out) :: h
  real(8) :: x, y
  x = theta(1)
  y = theta(2)
  h = sqrt((1-x)**2+100*(y-x**2)**2)
end subroutine fcn
